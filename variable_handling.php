<?php
//super global variable
$a = 15;
function addit() {
GLOBAL $a;
$a++;
print "Value  is now $a";
}
addit();
echo "</br>";

//Server super global variable
echo "</br>";
echo $_SERVER['PHP_SELF'];
echo "</br>";
echo $_SERVER['SERVER_NAME'];
echo "</br>";
echo $_SERVER['HTTP_HOST'];
echo "</br>";
echo $_SERVER['HTTP_USER_AGENT'];
echo "</br>";
echo $_SERVER['SCRIPT_NAME'];
echo "</br>";

$var = 0;

// Evaluates to true because $var is empty
if (empty($var)) {
    echo '$var is either 0, empty, or not set at all';
echo "</br>";
$yes = array('this', 'is', 'an array');

echo is_array($yes) ? 'Array' : 'not an Array';
echo "\n";
}
//this is is_array Example
$no = 'this is a string';
echo is_array($no) ? 'Array' : 'not an Array';
echo "</br>";
//this is isselt example
$b="hellow";
if (isset($b)) {
	echo "Value of b is set so i shall print";
}
echo "</br>";
//this is print_r example
$a = array ('a' => 'apple', 'b' => 'banana', 'c' => array ('x', 'y', 'z'));
print_r ($a);
echo "</br>";
//this is unset example
function foo()
{
    static $bar;
    $bar++;
    echo "Before unset: $bar, ";
    unset($bar);
    $bar = 23;
    echo "after unset: $bar\n";
}

foo();
echo "</br>";
foo();
echo "</br>";
foo();
echo "</br>";

//this is Gettype example
$data = array(1, 1., NULL, new stdClass, 'foo');

foreach ($data as $value) {
    echo gettype($value), "</br>";
}
echo "</br>";
//Example of is_bool


$a = false;
$b = 0;

// Since $a is a boolean, it will return true
if (is_bool($a) === true) {
    echo "Yes, this is a boolean";
}

// Since $b is not a boolean, it will return false
if (is_bool($b) === false) {
    echo "No, this is not a boolean";
}

?>

















