<?php
//integer type variable
$int_var = 12345;
$another_int = -12345 + 12345;
echo $int_var;
echo "</br>";
//float type variable
$many = 2.2888800;
$many_2 = 2.2111200;
echo $many;
echo "</br>";
//null type variable
$a=null;
echo $a;
echo "</br>";
//string type variable
$a= "This is a string with multiple characters ";
echo $a;
echo "</br>";



if (TRUE)// boolean type variable
print("This will always print<br>");
else
print("This will never print<br>");
$a=null;
echo $a;
echo "</br>";
$student=array("Rahim","karim","Tushar","Ananda");
echo "First student is ".$student[0]."</br>";
echo "Second student is ".$student[1]."</br>";
echo "third student is ".$student[2]."</br>";
echo "Fourth student is ".$student[3]."</br>";
echo "<hr/>";


/**
* object type variable
*/
class student
{
	
	function department()
	{
		return "CSE";
	}
	function details(){
		echo $this->department();
	}
}
$st = new student;// Creating Object from student class
$st->details();

?>