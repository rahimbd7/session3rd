<?php
$float_with_string="220.111BITM";
echo $float_with_string;
echo "</br>";
//empty function added
if (empty($var)) {
    echo '$var is either 0, empty, or not set at all';
    echo "</br>";
}
//is array function test
$yes = array('this', 'is', 'an array');

echo is_array($yes) ? 'Array' : 'not an Array';
echo "</br>";

//this is is_array Example
$no = 'this is a string';
echo is_array($no) ? 'Array' : 'not an Array';
echo "</br>";
//null type variable
$a="hellow";
$b=null;
echo var_dump(is_null($a));
echo"</br>";
echo var_dump(is_null($b));
echo "</br>";
/*
object type variable
*/
class student
{

    function department()
    {
        return "CSE";
    }
    function details(){
        echo $this->department();
    }
}


$st = new student;// Creating Object from student class
$st->details();
echo "</br>";
var_dump(is_object($st));
echo "</br>";

//isset functions is test

$arr=array("rahim","rasel","ruble");
$str= serialize($arr);
echo $str;

$before_value = unserialize($str);
echo "</br>";
print_r($before_value);
echo "</br>";

//isset function test
$b="hellow";
if (isset($b)) {
    echo "Value of b is set so i shall print";

}
echo "</br>";
//unset functions
function foo()
{
    static $bar;
    $bar++;
    echo "Before unset: $bar, ";
    unset($bar);
    $bar = 23;
    echo "after unset: $bar\n";
}

foo();
echo "</br>";
foo();
echo "</br>";
foo();
echo "</br>";
//var_dump function tested
$a = array(1, 2, array("a", "b", "c"));
var_dump($a);
echo "</br>";
//var_export has tested
$a = array (1, 2, array ("a", "b", "c"));
var_export($a);
echo"</br>";
//gettype function tested
$data = array(1, 1., NULL, new stdClass, 'foo');

foreach ($data as $value) {
    echo gettype($value), "</br>";
}
//Example of is_bool


$a = false;
$b = 0;

// Since $a is a boolean, it will return true
if (is_bool($a) === true) {
    echo "Yes, this is a boolean";
}
echo "</br>";
// Since $b is not a boolean, it will return false
if (is_bool($b) === false) {
    echo "No, this is not a boolean";
}







?>